<?php

//class
class robot {
    //property
    // public $suara ="ngik ngik";
    // public $berat = 100;

    public $suara;
    public $berat;

    //konstruktur
    public function __construct($suara,$berat){
        $this->suara=$suara;
        $this->berat=$berat;
    }

    public function set_suara($suara){
        $this->suara = $suara;
    }

    public function get_suara(){
        return $this->suara;
    }

    public function set_berat($berat){
        $this->berat = $berat;
    }

    public function get_berat(){
        return $this->berat;
    }

    //metode set dan get

    // public function bersuara (){
    //     echo "Suara Robotnya .... ".$this->suara;
    // }

    // public function berat_robot(){
    //     return $this->berat;
    // }
}


?>